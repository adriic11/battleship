
namespace WebApplicationChat.Models
{
    using System;
    using System.Collections.Generic;
    using WebApplicationChat.Datos;

    public partial class User
    {
        public int IDUsuario { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }

        public static Respuesta AgregarToken(User usuario)
        {
            List<Parametro> parametros = new List<Parametro>
            {
                new Parametro("@Nombre",usuario.Usuario),
                new Parametro("@Token",usuario.Token)

            };

            return DBDatos.Ejecutar("Agregar_Token", parametros);
        }
    }
}
