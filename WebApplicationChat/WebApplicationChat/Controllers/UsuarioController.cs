﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using WebApplicationChat.Datos;
using WebApplicationChat.Models;
using WebApplicationChat.Models.Solicitud;

namespace WebApplicationChat.Controllers
{
    public class UsuarioController : ApiController
    {
        [Route("api/Usuario/Registrarse")]
        [HttpPost]
        public IHttpActionResult Add(UsuarioSolicitud model)
        {
            using (HundirLaFlotaEntities bd = new HundirLaFlotaEntities())
            {
                var _Usuario = new User();
                _Usuario.Usuario = model.Usuario;
                _Usuario.Password = model.Password;
                bd.User.Add(_Usuario);
                bd.SaveChanges();
            }
            return Ok("exito");
        }

        [Route("api/Usuario/Login")]
        public IHttpActionResult Login(UsuarioSolicitud model)
        {
            Respuesta respuesta = new Respuesta();
            respuesta.resultado = 0;

            if (model == null)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
            using (HundirLaFlotaEntities bd = new HundirLaFlotaEntities())
            {
                var list = bd.User.Where(d => d.Usuario == model.Usuario && d.Password == model.Password);

                if (list.Count() > 0)
                {
                    respuesta.resultado = 1;
                    respuesta.data = TokenGenerator.GenerarToken(model.Usuario);

                    User user = list.First();
                    user.Token = (string)respuesta.data;

                    Models.User.AgregarToken(user);
                    return Ok(respuesta.data);
                }
                else
                {
                    return Unauthorized();
                }
            }
        }

        public bool ValidarToken(HttpRequestMessage request)
        {
            string token = "";

            foreach (var item in request.Headers)
            {
                if (item.Key.Equals("Authorization"))
                {
                    token = item.Value.First();
                    break;
                }
            }

            using (HundirLaFlotaEntities bd = new HundirLaFlotaEntities())
            {
                if (bd.User.Where(d => d.Token == token).Count() > 0)
                {
                    return true;
                }
            }

            return false;
        }
    }
}