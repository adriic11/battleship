﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Http;
using WebApplicationChat.Models;

namespace WebApplicationChat.Controllers
{
    public class ScoreController : ApiController
    {
        [HttpGet]
        public IHttpActionResult Get()
        {
            DataTable table = new DataTable();
            List<ViewModelScore.ModelScore> lst = new List<ViewModelScore.ModelScore>();
            using (HundirLaFlotaEntities bd = new HundirLaFlotaEntities())
            {
                string query = @"SELECT top 25 * FROM Puntuacion ORDER BY Puntos desc";
                using (var con = new SqlConnection(ConfigurationManager.ConnectionStrings["stringConexion"].ConnectionString))
                using (var cmd = new SqlCommand(query, con))
                using (var da = new SqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.Text;
                    da.Fill(table);
                }
               
            }
            return Ok(table);
        }
    }
}