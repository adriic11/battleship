﻿using System;

namespace ViewModelScore
{
    public class ModelScore
    {
        public int IDScore { get; set; }
        public string Nombre { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }
        public Nullable<int> Movimientos { get; set; }
        public Nullable<int> Puntuacion { get; set; }
    }
}
