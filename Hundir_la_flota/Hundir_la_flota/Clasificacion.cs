﻿using Hundir_la_flota.BBDD;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hundir_la_flota
{
    public partial class Clasificacion : Form
    {
        public Clasificacion()
        {
            InitializeComponent();
        }

        private async Task<string> GetHttp()
        {
            WebRequest request = WebRequest.Create("http://localhost:49759/api/Score");
            WebResponse response = request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            return await sr.ReadToEndAsync();
        }

        private async void Clasificacion_Load(object sender, EventArgs e)
        {
            string respuesta = await GetHttp();
            List<Puntuacion> lst =
                JsonConvert.DeserializeObject<List<Puntuacion>>(respuesta);
            dataGridView1.DataSource = lst;
        }
    }
}
