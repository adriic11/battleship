﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hundir_la_flota.Models
{
    class UsuarioLogin
    {
        public string Usuario { get; set; }
        public string Password { get; set; }

        public UsuarioLogin (string user)
        {
            Usuario = user;
        }

        public string getUsuario()
        {
            return this.Usuario;
        }

        public void setUsuario(string user)
        {
            this.Usuario = user;
        }
    }
}
