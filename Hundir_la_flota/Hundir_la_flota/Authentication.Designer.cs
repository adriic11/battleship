﻿namespace Hundir_la_flota
{
    partial class Authentication
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtToken = new System.Windows.Forms.TextBox();
            this.labelToken = new System.Windows.Forms.Label();
            this.panelClasificacion = new System.Windows.Forms.Panel();
            this.btnClasificacion = new System.Windows.Forms.Button();
            this.panelChat = new System.Windows.Forms.Panel();
            this.btnChat = new System.Windows.Forms.Button();
            this.btnValidar = new System.Windows.Forms.Button();
            this.labelUsuario = new System.Windows.Forms.Label();
            this.panelClasificacion.SuspendLayout();
            this.panelChat.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtToken
            // 
            this.txtToken.Location = new System.Drawing.Point(106, 43);
            this.txtToken.Name = "txtToken";
            this.txtToken.Size = new System.Drawing.Size(175, 20);
            this.txtToken.TabIndex = 0;
            // 
            // labelToken
            // 
            this.labelToken.AutoSize = true;
            this.labelToken.Location = new System.Drawing.Point(25, 46);
            this.labelToken.Name = "labelToken";
            this.labelToken.Size = new System.Drawing.Size(75, 13);
            this.labelToken.TabIndex = 1;
            this.labelToken.Text = "Authentication";
            // 
            // panelClasificacion
            // 
            this.panelClasificacion.Controls.Add(this.btnClasificacion);
            this.panelClasificacion.Location = new System.Drawing.Point(1, 96);
            this.panelClasificacion.Name = "panelClasificacion";
            this.panelClasificacion.Size = new System.Drawing.Size(200, 209);
            this.panelClasificacion.TabIndex = 2;
            // 
            // btnClasificacion
            // 
            this.btnClasificacion.Location = new System.Drawing.Point(59, 95);
            this.btnClasificacion.Name = "btnClasificacion";
            this.btnClasificacion.Size = new System.Drawing.Size(75, 23);
            this.btnClasificacion.TabIndex = 0;
            this.btnClasificacion.Text = "Puntuación";
            this.btnClasificacion.UseVisualStyleBackColor = true;
            this.btnClasificacion.Click += new System.EventHandler(this.btnClasificacion_Click);
            // 
            // panelChat
            // 
            this.panelChat.Controls.Add(this.btnChat);
            this.panelChat.Location = new System.Drawing.Point(207, 96);
            this.panelChat.Name = "panelChat";
            this.panelChat.Size = new System.Drawing.Size(200, 209);
            this.panelChat.TabIndex = 3;
            // 
            // btnChat
            // 
            this.btnChat.Location = new System.Drawing.Point(63, 95);
            this.btnChat.Name = "btnChat";
            this.btnChat.Size = new System.Drawing.Size(75, 23);
            this.btnChat.TabIndex = 1;
            this.btnChat.Text = "Chat";
            this.btnChat.UseVisualStyleBackColor = true;
            this.btnChat.Click += new System.EventHandler(this.btnChat_Click);
            // 
            // btnValidar
            // 
            this.btnValidar.Location = new System.Drawing.Point(287, 41);
            this.btnValidar.Name = "btnValidar";
            this.btnValidar.Size = new System.Drawing.Size(75, 23);
            this.btnValidar.TabIndex = 4;
            this.btnValidar.Text = "Validar";
            this.btnValidar.UseVisualStyleBackColor = true;
            this.btnValidar.Click += new System.EventHandler(this.btnValidar_Click);
            // 
            // labelUsuario
            // 
            this.labelUsuario.AutoSize = true;
            this.labelUsuario.Location = new System.Drawing.Point(167, 13);
            this.labelUsuario.Name = "labelUsuario";
            this.labelUsuario.Size = new System.Drawing.Size(43, 13);
            this.labelUsuario.TabIndex = 5;
            this.labelUsuario.Text = "Usuario";
            // 
            // Authentication
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 305);
            this.Controls.Add(this.labelUsuario);
            this.Controls.Add(this.btnValidar);
            this.Controls.Add(this.panelChat);
            this.Controls.Add(this.panelClasificacion);
            this.Controls.Add(this.labelToken);
            this.Controls.Add(this.txtToken);
            this.Name = "Authentication";
            this.Text = "Authentication";
            this.panelClasificacion.ResumeLayout(false);
            this.panelChat.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtToken;
        private System.Windows.Forms.Label labelToken;
        private System.Windows.Forms.Panel panelClasificacion;
        private System.Windows.Forms.Button btnClasificacion;
        private System.Windows.Forms.Panel panelChat;
        private System.Windows.Forms.Button btnChat;
        private System.Windows.Forms.Button btnValidar;
        public System.Windows.Forms.Label labelUsuario;
    }
}