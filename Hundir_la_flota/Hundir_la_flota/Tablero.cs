﻿using Hundir_la_flota.BBDD;
using Hundir_la_flota.TiposBarco;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hundir_la_flota
{
    public partial class Tablero : Form
    {
        private Casilla[,] tablero;
        private Barco[] barcos = new Barco[5];
        public int fila, columna;
        private int movimientos,puntos;
        Random aleatorio = new Random();
        DateTime today = DateTime.Now;
        HundirLaFlotaEntities con = new HundirLaFlotaEntities();
        Puntuacion pts;

        public Tablero(int fila, int col)
        {
            InitializeComponent();
            this.fila = fila;
            this.columna = col;

            barcos[0] = new Lancha();
            //barcos[1] = new Lancha();
            //barcos[2] = new Lancha();
            //barcos[3] = new Lancha();
            barcos[1] = new Fragata();
            //barcos[5] = new Fragata();
            //barcos[6] = new Fragata();
            barcos[2] = new Buque();
            //barcos[3] = new Buque();
            barcos[3] = new Portaaviones();
            barcos[4] = new Catamaran();
        }

        private void Tablero_Load(object sender, EventArgs e)
        { 
            CrearTablero(10, 10);
            Generar();
            _panelPuntuacion.Visible = false;
            _panelP1.Visible = false;
        }
        private void NumeroMovimientos()
        {
            movimientos++;
            labelMovimientos.Text = movimientos.ToString();
        }
        public void CrearTablero(int fila, int col)
        {
            tablero = new Casilla[fila, col];
            for (int f = 0; f < fila; f++)
                for (int c = 0; c < col; c++)
                {
                    tablero[f, c] = new Casilla(panelTablero);
                    tablero[f, c].setFila(f);
                    tablero[f, c].setColumna(c);
                    tablero[f, c].Posicion(55 * f, 55 * c);
                    tablero[f, c].Dimension(55, 55);
                    tablero[f, c].SizeMode = PictureBoxSizeMode.StretchImage;
                    tablero[f, c].BorderStyle = BorderStyle.Fixed3D;
                    tablero[f, c].BackColor = Color.White;
                    tablero[f, c].Visible = true;
                    tablero[f, c].BringToFront();
                    tablero[f, c].Click += CasillaClick;
                    tablero[f, c].agua = true;
                    tablero[f, c].barco = false;
                }
            this.Refresh();
        } 

        private void CasillaClick(object sender, EventArgs e)
        {
            int columna;
            int fila;
            fila = ((Casilla)sender).getFila();
            columna = ((Casilla)sender).getColumna();
            PintarFicha(fila, columna);
            NumeroMovimientos();
            ComprobarBarco(fila, columna);
            CalcularPuntuacion();
        }
       
        private bool PonerBarco(int barco, int fila, int columna)
        {
            bool ocupado = false;
            int contadorV = -1;
            int contadorH = -1;
            int orientacion = aleatorio.Next(0, 2);

            // colocacion horizontal
            if ( orientacion == 0 )
            {
                if (columna + barco + 1 > 10)
                    return false;

                for (int i = columna; i <= columna + barco; i++)
                {
                    if (tablero[fila,i].agua == true)
                    {
                        contadorH++;
                    }
                }

                for (int i = columna; i <= columna + barco; i++)
                {
                    if (contadorH == barco)
                    {
                        //tablero[fila, i].BackColor = Color.Red;
                        tablero[fila, i].barco = true;
                        tablero[fila, i].agua = false;
                        barcos[barco].CasillasBarco[i - columna] = tablero[fila, i];                     
                    }
                    else
                    {
                        ocupado = true;
                    }
                }
            }

            // colocacion vertical
            if (orientacion == 1)
            {
                if (fila + barco + 1 > 10)
                    return false;
                for (int i = fila; i <= fila + barco; i++)
                {
                    if (tablero[i,columna].agua == true)
                    {
                        contadorV++;
                    }
                }

                for (int i = fila; i <= fila + barco; i++)
                {
                    if (contadorV == barco)
                    {
                        //tablero[i, columna].BackColor = Color.Red;
                        tablero[i, columna].barco = true;
                        tablero[i, columna].agua = false;                        
                        barcos[barco].CasillasBarco[i - fila] = tablero[i, columna];                      
                    }
                    else
                    {
                        ocupado = true;
                    }
                }
            }
            if (ocupado)
            {
                return false;
            }
            return true;
        }

        private void Generar()
        {
            int barco, fila, columna;
            bool creado;

            for (int i = 0; i < 5; i++)
            {
                barco = i;
                fila = aleatorio.Next(0, 9);
                columna = aleatorio.Next(0, 9);
                creado = PonerBarco(barco, fila, columna) ? true : false;

                if (!creado)
                {
                    i--;
                }
            }
        } 

        private void PintarFicha(int fila, int col)
        {
            if (tablero[fila, col].barco == true)
            {
                tablero[fila, col].BackColor = Color.Green;
                this.Refresh();
            }
            else
            {
                tablero[fila, col].BackColor = Color.Turquoise;
                this.Refresh();
            }
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            _panelPuntuacion.Visible = false;
        }

        private void btnNewGame_Click(object sender, EventArgs e)
        {
            CrearTablero(10, 10);
            Generar();
            _panelPuntuacion.Visible = false;
            movimientos = 0;
        }

        private void btnHome_Click(object sender, EventArgs e)
        {
            Form1 f = new Form1();
            f.Show();
            Dispose();
        }

        private void buttonPuntuacionP1_Click(object sender, EventArgs e)
        {
            _panelP1.Visible = false;
            _panelPuntuacion.Visible = true;
            _panelPuntuacion.BringToFront();
            //CalcularPuntuacion();
            string nombre = textBoxNombreP1.Text;
            labelNombre.Text = nombre;
            string mov = Convert.ToString(movimientos);
            labelMoves.Text = mov;
            labelPuntos.Text = puntos + " Pts";

            pts = new Puntuacion();
            pts.Nombre = textBoxNombreP1.Text;
            pts.Fecha = today;
            pts.Movimientos = movimientos;
            pts.Puntos = puntos;

            con.Puntuacion.Add(pts);
            con.SaveChanges();
        }

        private void CalcularPuntuacion()
        {
            if (movimientos <= 60)
            {
                puntos = 15;
            }
            else if (movimientos > 60 && movimientos < 70)
            {
                puntos = 10;
            }
            else if (movimientos > 70 && movimientos < 80)
            {
                puntos = 5;
            }
            else if (movimientos > 80)
            {
                puntos = 0;
                _panelP1.Visible = true;
                _panelP1.BringToFront();
                labelP1.Text = "DERROTA";
            }
        }

        private void ComprobarBarco(int fila, int col)
        {
            for (int i = 0; i < barcos.Length; i++)
            {
                for (int j = 0; j < barcos[i].CasillasBarco.Length; j++)
                {
                    if (barcos[i].CasillasBarco[j] == tablero[fila, col])
                    {
                        if (barcos[i].Hundido())
                        {
                            MessageBox.Show("Tocado y Hundido");
                        }
                        else
                            MessageBox.Show("Tocado");
                    }
                }
            }
            int contador = 0;
            for (int i = 0; i < barcos.Length; i++)
            {
                if (barcos[i].Hundido())
                {
                    contador++;
                }
            }
            if (contador == 5)
            {     
                _panelP1.Visible = true;
                _panelP1.BringToFront();
            }
        }

    }
}
