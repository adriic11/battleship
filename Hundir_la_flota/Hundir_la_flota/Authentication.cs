﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Windows.Forms;
using Hundir_la_flota.BBDD;
using Hundir_la_flota.Models;
using Hundir_la_flota.Usuario;

namespace Hundir_la_flota
{
    public partial class Authentication : Form
    {
        public Authentication()
        {
            InitializeComponent();
            panelChat.Visible = false;
            panelClasificacion.Visible = false;
        }

        private void btnValidar_Click(object sender, EventArgs e)
        {
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["stringConexion"].ConnectionString);
            string user = labelUsuario.Text;
            
            if (txtToken.Text != string.Empty)
            {
                string query = @"SELECT * FROM [HundirLaFlota].[dbo].[User] WHERE Token = '"
                    + txtToken.Text + "' AND Usuario = '" + user + "'" ;
                var cmd = new SqlCommand(query, con);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    dr.Close();
                    panelClasificacion.Visible = true;
                    panelChat.Visible = true;
                    MessageBox.Show("Usuario autentificado.", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    dr.Close();
                    MessageBox.Show("Token incorrecto. ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
                MessageBox.Show("Porfavor ingrese el valor en el campo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void btnClasificacion_Click(object sender, EventArgs e)
        {
            this.Hide();
            Clasificacion pts = new Clasificacion();
            pts.Show();
        }

        private void btnChat_Click(object sender, EventArgs e)
        {
            ChatUser chat = new ChatUser();
            chat.labelUsuario.Text = labelUsuario.Text;
            chat.Show();
        }
    }
}
