﻿using Hundir_la_flota.BBDD;
using Hundir_la_flota.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hundir_la_flota.Usuario
{
    public partial class Login : Form
    {
        User user = new User();
        
        public Login()
        {
            InitializeComponent();
            txtPass.PasswordChar = '*';
        }

        private void checkBoxMostrar_CheckedChanged(object sender, EventArgs e)
        {
            char c = new char();
            if (checkBoxMostrar.Checked)
                txtPass.PasswordChar = c;
            else
                txtPass.PasswordChar = '*'; 
        }

        private void labelRegistro_Click(object sender, EventArgs e)
        {
            Registro registro = new Registro();
            
            registro.Show();
        }

        private void buttonLogin_Click(object sender, EventArgs e)
        {
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["stringConexion"].ConnectionString);
            errorProvider1.SetError(txtUser, "");
            errorProvider1.SetError(txtPass, "");
            if (txtUser.Text != string.Empty && txtPass.Text != string.Empty)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://localhost:49759/");
                    User login = new User { Usuario = txtUser.Text.ToString(), Password = txtPass.Text.ToString() };
                    
                    var response = client.PostAsJsonAsync("api/Usuario/Login", login).Result;
                    var a = response.Content.ReadAsStringAsync();

                    string query = @"SELECT * FROM [HundirLaFlota].[dbo].[User] WHERE Usuario = '" + txtUser.Text +
                        "' AND Password = '" + txtPass.Text + "'";
                    var cmd = new SqlCommand(query, con);
                    con.Open();
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        dr.Close();
                        this.Hide();
                        Authentication auth = new Authentication();
                        auth.labelUsuario.Text = txtUser.Text;
                        auth.Show();
                        ChatUser user = new ChatUser();
                        user.labelUsuario.Text = txtUser.Text;
                        MessageBox.Show("Has iniciado sesión.", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        dr.Close();
                        MessageBox.Show("Nombre de usuario o contraseña incorrecta. ", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
                MessageBox.Show("Porfavor ingrese el valor en todos los campos.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
    

}
