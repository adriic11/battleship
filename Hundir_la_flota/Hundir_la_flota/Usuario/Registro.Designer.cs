﻿namespace Hundir_la_flota.Usuario
{
    partial class Registro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxUsuario = new System.Windows.Forms.TextBox();
            this.textBoxPass = new System.Windows.Forms.TextBox();
            this.btnAtras = new System.Windows.Forms.Button();
            this.btnRegis = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxConfirmPass = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBoxMostrarConfirm = new System.Windows.Forms.CheckBox();
            this.checkBoxMostrarPass = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxUsuario
            // 
            this.textBoxUsuario.Location = new System.Drawing.Point(28, 43);
            this.textBoxUsuario.Name = "textBoxUsuario";
            this.textBoxUsuario.Size = new System.Drawing.Size(113, 20);
            this.textBoxUsuario.TabIndex = 0;
            // 
            // textBoxPass
            // 
            this.textBoxPass.Location = new System.Drawing.Point(28, 93);
            this.textBoxPass.Name = "textBoxPass";
            this.textBoxPass.Size = new System.Drawing.Size(113, 20);
            this.textBoxPass.TabIndex = 1;
            // 
            // btnAtras
            // 
            this.btnAtras.Location = new System.Drawing.Point(122, 181);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 2;
            this.btnAtras.Text = "Atrás";
            this.btnAtras.UseVisualStyleBackColor = true;
            // 
            // btnRegis
            // 
            this.btnRegis.Location = new System.Drawing.Point(28, 181);
            this.btnRegis.Name = "btnRegis";
            this.btnRegis.Size = new System.Drawing.Size(75, 23);
            this.btnRegis.TabIndex = 3;
            this.btnRegis.Text = "Registrarse";
            this.btnRegis.UseVisualStyleBackColor = true;
            this.btnRegis.Click += new System.EventHandler(this.btnRegis_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Usuario";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Contraseña";
            // 
            // textBoxConfirmPass
            // 
            this.textBoxConfirmPass.Location = new System.Drawing.Point(28, 145);
            this.textBoxConfirmPass.Name = "textBoxConfirmPass";
            this.textBoxConfirmPass.Size = new System.Drawing.Size(113, 20);
            this.textBoxConfirmPass.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Confirmar contraseña";
            // 
            // checkBoxMostrarConfirm
            // 
            this.checkBoxMostrarConfirm.AutoSize = true;
            this.checkBoxMostrarConfirm.Location = new System.Drawing.Point(161, 145);
            this.checkBoxMostrarConfirm.Name = "checkBoxMostrarConfirm";
            this.checkBoxMostrarConfirm.Size = new System.Drawing.Size(61, 17);
            this.checkBoxMostrarConfirm.TabIndex = 8;
            this.checkBoxMostrarConfirm.Text = "Mostrar";
            this.checkBoxMostrarConfirm.UseVisualStyleBackColor = true;
            this.checkBoxMostrarConfirm.CheckedChanged += new System.EventHandler(this.checkBoxMostrarConfirm_CheckedChanged);
            // 
            // checkBoxMostrarPass
            // 
            this.checkBoxMostrarPass.AutoSize = true;
            this.checkBoxMostrarPass.Location = new System.Drawing.Point(161, 96);
            this.checkBoxMostrarPass.Name = "checkBoxMostrarPass";
            this.checkBoxMostrarPass.Size = new System.Drawing.Size(61, 17);
            this.checkBoxMostrarPass.TabIndex = 9;
            this.checkBoxMostrarPass.Text = "Mostrar";
            this.checkBoxMostrarPass.UseVisualStyleBackColor = true;
            this.checkBoxMostrarPass.CheckedChanged += new System.EventHandler(this.checkBoxMostrarPass_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.checkBoxMostrarPass);
            this.panel1.Controls.Add(this.textBoxUsuario);
            this.panel1.Controls.Add(this.checkBoxMostrarConfirm);
            this.panel1.Controls.Add(this.textBoxPass);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btnAtras);
            this.panel1.Controls.Add(this.textBoxConfirmPass);
            this.panel1.Controls.Add(this.btnRegis);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(46, 56);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(225, 223);
            this.panel1.TabIndex = 10;
            // 
            // Registro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(335, 343);
            this.Controls.Add(this.panel1);
            this.Name = "Registro";
            this.Text = "Registro";
            this.Load += new System.EventHandler(this.Registro_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox textBoxUsuario;
        private System.Windows.Forms.TextBox textBoxPass;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Button btnRegis;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxConfirmPass;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox checkBoxMostrarConfirm;
        private System.Windows.Forms.CheckBox checkBoxMostrarPass;
        private System.Windows.Forms.Panel panel1;
    }
}