﻿using Hundir_la_flota.BBDD;
using Hundir_la_flota.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace Hundir_la_flota.Usuario
{
    public partial class Registro : Form
    {
        
        
        public Registro()
        {
            InitializeComponent();
            textBoxPass.PasswordChar = '*';
            textBoxConfirmPass.PasswordChar = '*';
        }

        public string Enviar<T>(string url, T objectRequest, string method = "POST")
        {
            string result = "";

            try
            {

                JavaScriptSerializer js = new JavaScriptSerializer();

                string json = Newtonsoft.Json.JsonConvert.SerializeObject(objectRequest);

                // Peticion
                WebRequest request = WebRequest.Create(url);
                // Header
                request.Method = method;
                request.PreAuthenticate = true;
                request.ContentType = "application/json;charset=utf-8'";

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        private void btnRegis_Click(object sender, EventArgs e)
        {
            string url = "http://localhost:49759/api/Usuario/Registrarse";
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["stringConexion"].ConnectionString);
            DataTable table = new DataTable();

            if (textBoxUsuario.Text != string.Empty || textBoxConfirmPass.Text != string.Empty || textBoxPass.Text != string.Empty)
            {
                if (textBoxPass.Text == textBoxConfirmPass.Text)
                {
                    string query = @"SELECT * FROM [HundirLaFlota].[dbo].[User] WHERE Usuario = '" + textBoxUsuario.Text + "'";
                    con.Open();
                    var cmd = new SqlCommand(query, con);
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        dr.Close();
                        MessageBox.Show("El usuario ya existe, introduzca otro porfavor.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        User user = new User();
                        user.Usuario = textBoxUsuario.Text;
                        user.Password = textBoxPass.Text;
                        string resultado = Enviar<User>(url, user, "POST");
                        MessageBox.Show("Tu cuenta ha sido creada. Inicia sesión.", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    MessageBox.Show("Las contraseñas no coinciden.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Porfavor, introduzca un valor en el campo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Registro_Load(object sender, EventArgs e)
        {
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["stringConexion"].ConnectionString);
        }

        private void checkBoxMostrarPass_CheckedChanged(object sender, EventArgs e)
        {
            char c = new char();
            if (checkBoxMostrarPass.Checked)
            {
                textBoxPass.PasswordChar = c;
            }
            else
            {
                textBoxPass.PasswordChar = '*';
            }
        }

        private void checkBoxMostrarConfirm_CheckedChanged(object sender, EventArgs e)
        {
            char c = new char();
            if (checkBoxMostrarConfirm.Checked)
            {
                textBoxConfirmPass.PasswordChar = c;
            }
            else
            {
                textBoxConfirmPass.PasswordChar = '*';
            }

        }
    }
}
