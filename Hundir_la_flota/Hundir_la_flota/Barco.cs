﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hundir_la_flota
{
    abstract class Barco 
    {
        protected Casilla[] casillaBarco;

        public Barco(int tamano)
        {
            casillaBarco = new Casilla[tamano];
        }

        public Casilla[] CasillasBarco { get => casillaBarco; set => casillaBarco = value; }

        public bool Hundido()
        {
            int contador = 0;

            for (int i=0; i< casillaBarco.Length; i++)
            {
                if (casillaBarco[i].BackColor == Color.Green)
                   contador++;
            }

            if (contador == casillaBarco.Length)
                return true;
            else
                return false;
        }
       
        
    }
}
