﻿using Hundir_la_flota.BBDD;
using Hundir_la_flota.Usuario;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace Hundir_la_flota
{
    public partial class ChatUser : Form
    {
        Chat chat = new Chat();

        public ChatUser()
        {
            
            InitializeComponent();
            panelChat.Visible = false; 
        }

        public string Enviar<T>(string url, T objectRequest, string method = "POST")
        {
            string result = "";

            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();

                string json = JsonConvert.SerializeObject(objectRequest);

                // Peticion
                WebRequest request = WebRequest.Create(url);
                // Header
                request.Method = method;
                request.PreAuthenticate = true;
                request.ContentType = "application/json;charset=utf-8'";

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    streamWriter.Write(json);
                    streamWriter.Flush();
                }

                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

            }
            catch (Exception e)
            {
                result = e.Message;
            }
            return result;
        }

        private void btnCanal_Click(object sender, EventArgs e)
        {
            string url = "http://localhost:49759/api/Usuario/Canal";
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["stringConexion"].ConnectionString);
            DataTable table = new DataTable();
         
            if (txtCanal.Text != string.Empty)
            {
                Chat chat = new Chat();
                chat.Canal = txtCanal.Text;
                string resultado = Enviar<Chat>(url, chat, "POST");
                MessageBox.Show("Te has unido al canal.", "Hecho", MessageBoxButtons.OK, MessageBoxIcon.Information);
                panelChat.Visible = true;
            }
            else
            {
                MessageBox.Show("Porfavor, introduzca un valor en el campo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            string url = "http://localhost:49759/api/Usuario/Canal/Enviar";
            var con = new SqlConnection(ConfigurationManager.ConnectionStrings["stringConexion"].ConnectionString);
            DataTable table = new DataTable();

            if (txtCanal.Text != string.Empty)
            {
                DateTime tiempo = DateTime.Now;
                           
                chat.Usuario = labelUsuario.Text;
                chat.Canal = txtCanal.Text;
                chat.Mensaje = txtMensaje.Text;
                chat.Fecha = tiempo;

                string resultado = Enviar<Chat>(url, chat, "POST");
                MostrarMensaje(chat.Usuario, chat.Mensaje);
                
                //var listaMensaje = JsonConvert.DeserializeObject<List<Chat>>(resultado);

                //foreach (Chat ch in listaMensaje)
                //{
                //    txtChat.Text = ("Nombre: " + chat.Usuario + "\r\n Mensaje: " + chat.Mensaje);
                //}
                
            }
            else
            {
                MessageBox.Show("Porfavor, introduzca un valor en el campo.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }       
          
        }

        public void MostrarMensaje (string nombre, string mensaje)
        {

            //\"IDCanal\": \"0\", \"Canal\": "+txtCanal.Text+", "[{ \"Usuario\": " + nombre + "\r\n, " +
            //"\"Mensaje\": " + mensaje + "\r\n ," +
            //"\"Fecha\": " + fecha + "\r\n}]";
            //var chat = "[{ \"Usuario\": \"nombre\", " +
            //"\"Mensaje\": \"mensaje\", " +
            //"\"Fecha\": \"fecha\"}]";
            var msn = "[{\"Usuario\": \"" + nombre + "\", \"Mensaje\" : \"" + mensaje + "\" }]";//, \"Fecha\": \""+fecha+"\"}]";
            var listaMensaje = JsonConvert.DeserializeObject<List<Chat>>(msn);

            foreach (Chat chat in listaMensaje)
            {
                txtChat.Text = ("Nombre: " + chat.Usuario + " - Mensaje: " + chat.Mensaje);
            }

        }

        private void ChatUser_Load(object sender, EventArgs e)
        {
          

        }

        //private async void MostrarChat ()
        //{
        //    string respuesta = await GetHttp();
        //    List<Chat> lst =
        //        JsonConvert.DeserializeObject<List<Chat>>(respuesta);
        //    txtChat.Text = ("Nombre: " + lst.Usuario + "\r\n Mensaje: " + chat.Mensaje);
        //    //Chat chat;
        //    //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@"http://localhost:49759/api/Usuario/Canal/Mostrar/" + txtCanal.Text+"");
        //    //using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        //    //using (Stream stream = response.GetResponseStream())
        //    //using (StreamReader reader = new StreamReader(stream))
        //    //{
        //    //    var json = reader.ReadToEnd();
        //    //    chat = JsonConvert.DeserializeObject<Chat>(json);
        //    //}
        //    //txtChat.Text = ("Nombre: " + chat.Usuario + "\r\n Mensaje: " + chat.Mensaje);

        //}

        //private async Task<string> GetHttp()
        //{
        //    WebRequest request = WebRequest.Create("http://localhost:49759/api/Usuario/Canal/Mostrar/" + txtCanal.Text+"");
        //    WebResponse response = request.GetResponse();
        //    StreamReader sr = new StreamReader(response.GetResponseStream());
        //    return await sr.ReadToEndAsync();
        //}

       
    }
}
