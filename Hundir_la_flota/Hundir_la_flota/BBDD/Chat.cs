
namespace Hundir_la_flota.BBDD
{
    using System;
    using System.Collections.Generic;
    
    public partial class Chat
    {
        public int IDChat { get; set; }
        public string Canal { get; set; }
        public string Usuario { get; set; }
        public string Mensaje { get; set; }
        public Nullable<System.DateTime> Fecha { get; set; }


    }
}
