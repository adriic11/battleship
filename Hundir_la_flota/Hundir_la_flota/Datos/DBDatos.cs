﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using static System.Configuration.ConfigurationManager;

namespace Hundir_la_flota.Datos
{
    class DBDatos
    {
        public static string preconex = ConnectionStrings["stringConexion"].ConnectionString;
        public static Respuesta Ejecutar(string nombreProcedimiento, List<Parametro> parametros, string stringConexion = "")
        {
            Respuesta respuesta = new Respuesta();
            respuesta.mensaje = "";
            SqlConnection conexion = new SqlConnection(string.IsNullOrEmpty(stringConexion) ? preconex : stringConexion);
            conexion.Open();

            try
            {
                SqlCommand cmd = new SqlCommand(nombreProcedimiento, conexion);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                if (parametros != null)
                {
                    foreach (var parametro in parametros)
                    {
                        if (!parametro.Salida)
                        {
                            cmd.Parameters.AddWithValue(parametro.Nombre, parametro.Valor);
                        }
                        else
                        {
                            cmd.Parameters.Add(parametro.Nombre, SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                        }
                    }
                }

                int e = cmd.ExecuteNonQuery();

                for (int i = 0; i < parametros.Count; i++)
                {
                    if (cmd.Parameters[i].Direction == ParameterDirection.Output)
                    {
                        string mensaje = cmd.Parameters[i].Value.ToString();

                        if (!string.IsNullOrEmpty(mensaje))
                        {
                            respuesta.mensaje = mensaje;
                        }
                    }
                }

                respuesta.exito = e > 0 ? true : false;
                respuesta.mensaje = e > 0 ? "Datos enviados" : "No se encontro el comprobante para actualizarlo";
            }
            catch (Exception e)
            {
                respuesta.exito = false;
                respuesta.mensaje = e.Message;
            }
            finally
            {
                conexion.Close();
            }

            return respuesta;
        }

        public static Respuesta Listar(string nombreProcedimiento, List<Parametro> parametros = null, string stringConexion = "")
        {
            Respuesta respuesta = new Respuesta();

            SqlConnection conexion = new SqlConnection(string.IsNullOrEmpty(stringConexion) ? preconex : stringConexion);
            conexion.Open();

            try
            {
                SqlCommand cmd = new SqlCommand(nombreProcedimiento, conexion);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;

                if (parametros != null)
                {
                    foreach (var parametro in parametros)
                    {
                        cmd.Parameters.AddWithValue(parametro.Nombre, parametro.Valor);
                    }
                }
                DataTable tabla = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(tabla);

                respuesta.exito = true;
                respuesta.mensaje = tabla.Rows.Count > 0 ? "Datos encontrados" : "Datos no encontrados";
                respuesta.resultado = tabla;
                return respuesta;
            }
            catch (Exception e)
            {
                respuesta.exito = false;
                respuesta.mensaje = e.Message;
                return respuesta;
            }
            finally
            {
                conexion.Close();
            }
        }
    }
}

