﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hundir_la_flota.Datos
{
    public class Respuesta
    {
        public string estado { get; set; }
        public bool exito { get; set; }
        public string mensaje { get; set; }
        public dynamic resultado { get; set; }
        public object data { get; set; }

        public Respuesta()
        {
            estado = "realizado";
        }

        public static Respuesta retornar(bool _exito, string _mensaje, dynamic _resultado)
        {
            Respuesta res = new Respuesta
            {
                estado = "realizado",
                exito = _exito,
                mensaje = _mensaje,
                resultado = _resultado
            };

            return res;
        }
    }
}
