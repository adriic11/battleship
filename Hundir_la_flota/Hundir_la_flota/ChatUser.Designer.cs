﻿namespace Hundir_la_flota
{
    partial class ChatUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtChat = new System.Windows.Forms.TextBox();
            this.txtMensaje = new System.Windows.Forms.TextBox();
            this.btnEnviar = new System.Windows.Forms.Button();
            this.btnCanal = new System.Windows.Forms.Button();
            this.txtCanal = new System.Windows.Forms.TextBox();
            this.labelCanal = new System.Windows.Forms.Label();
            this.panelChat = new System.Windows.Forms.Panel();
            this.labelUsuario = new System.Windows.Forms.Label();
            this.panelChat.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtChat
            // 
            this.txtChat.Location = new System.Drawing.Point(16, 19);
            this.txtChat.Multiline = true;
            this.txtChat.Name = "txtChat";
            this.txtChat.Size = new System.Drawing.Size(255, 224);
            this.txtChat.TabIndex = 0;
            // 
            // txtMensaje
            // 
            this.txtMensaje.Location = new System.Drawing.Point(16, 268);
            this.txtMensaje.Multiline = true;
            this.txtMensaje.Name = "txtMensaje";
            this.txtMensaje.Size = new System.Drawing.Size(157, 21);
            this.txtMensaje.TabIndex = 1;
            // 
            // btnEnviar
            // 
            this.btnEnviar.Location = new System.Drawing.Point(196, 258);
            this.btnEnviar.Name = "btnEnviar";
            this.btnEnviar.Size = new System.Drawing.Size(75, 31);
            this.btnEnviar.TabIndex = 2;
            this.btnEnviar.Text = "Enviar";
            this.btnEnviar.UseVisualStyleBackColor = true;
            this.btnEnviar.Click += new System.EventHandler(this.btnEnviar_Click);
            // 
            // btnCanal
            // 
            this.btnCanal.Location = new System.Drawing.Point(208, 25);
            this.btnCanal.Name = "btnCanal";
            this.btnCanal.Size = new System.Drawing.Size(75, 23);
            this.btnCanal.TabIndex = 3;
            this.btnCanal.Text = "Entrar";
            this.btnCanal.UseVisualStyleBackColor = true;
            this.btnCanal.Click += new System.EventHandler(this.btnCanal_Click);
            // 
            // txtCanal
            // 
            this.txtCanal.Location = new System.Drawing.Point(65, 25);
            this.txtCanal.Name = "txtCanal";
            this.txtCanal.Size = new System.Drawing.Size(137, 20);
            this.txtCanal.TabIndex = 4;
            // 
            // labelCanal
            // 
            this.labelCanal.AutoSize = true;
            this.labelCanal.Location = new System.Drawing.Point(25, 30);
            this.labelCanal.Name = "labelCanal";
            this.labelCanal.Size = new System.Drawing.Size(34, 13);
            this.labelCanal.TabIndex = 5;
            this.labelCanal.Text = "Canal";
            // 
            // panelChat
            // 
            this.panelChat.BackColor = System.Drawing.SystemColors.Control;
            this.panelChat.Controls.Add(this.txtChat);
            this.panelChat.Controls.Add(this.txtMensaje);
            this.panelChat.Controls.Add(this.btnEnviar);
            this.panelChat.Location = new System.Drawing.Point(12, 63);
            this.panelChat.Name = "panelChat";
            this.panelChat.Size = new System.Drawing.Size(293, 308);
            this.panelChat.TabIndex = 6;
            // 
            // labelUsuario
            // 
            this.labelUsuario.AutoSize = true;
            this.labelUsuario.Location = new System.Drawing.Point(116, 9);
            this.labelUsuario.Name = "labelUsuario";
            this.labelUsuario.Size = new System.Drawing.Size(35, 13);
            this.labelUsuario.TabIndex = 7;
            this.labelUsuario.Text = "label2";
            // 
            // ChatUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(317, 383);
            this.Controls.Add(this.labelUsuario);
            this.Controls.Add(this.panelChat);
            this.Controls.Add(this.labelCanal);
            this.Controls.Add(this.txtCanal);
            this.Controls.Add(this.btnCanal);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Name = "ChatUser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chat";
            this.Load += new System.EventHandler(this.ChatUser_Load);
            this.panelChat.ResumeLayout(false);
            this.panelChat.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtChat;
        private System.Windows.Forms.TextBox txtMensaje;
        private System.Windows.Forms.Button btnEnviar;
        private System.Windows.Forms.Button btnCanal;
        private System.Windows.Forms.TextBox txtCanal;
        private System.Windows.Forms.Label labelCanal;
        private System.Windows.Forms.Panel panelChat;
        public System.Windows.Forms.Label labelUsuario;
    }
}