﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hundir_la_flota
{
    class Casilla : PictureBox
    {
        private int fila, columna;
        public bool barco;
        public bool agua;

        public Casilla (Control padre)
        {
            this.Parent = padre;
        }

        public void Posicion ( int top, int left)
        {
            this.Top = top;
            this.Left = left;
        }

        public void Dimension (int x, int y)
        {
            this.Height = x;
            this.Width = y;
        }

        public int getFila()
        {
            return this.fila;
        }

        public int getColumna()
        {
            return this.columna;
        }

        public void setFila(int px)
        {
            this.fila = px;
        }

        public void setColumna(int py)
        {
            this.columna = py;
        }

    }
}
