﻿namespace Hundir_la_flota
{
    partial class Tablero
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelTablero = new System.Windows.Forms.Panel();
            this._panelPuntuacion = new System.Windows.Forms.Panel();
            this.panelPuntuacion = new System.Windows.Forms.Panel();
            this.labelNombre = new System.Windows.Forms.Label();
            this.labelMoves = new System.Windows.Forms.Label();
            this.btnNewGame = new System.Windows.Forms.Button();
            this.btnHome = new System.Windows.Forms.Button();
            this.btnAtras = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.labelMovimientos = new System.Windows.Forms.Label();
            this._panelP1 = new System.Windows.Forms.Panel();
            this.panelP1 = new System.Windows.Forms.Panel();
            this.buttonPuntuacionP1 = new System.Windows.Forms.Button();
            this.textBoxNombreP1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelP1 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.labelPuntos = new System.Windows.Forms.Label();
            this.panelTablero.SuspendLayout();
            this._panelPuntuacion.SuspendLayout();
            this.panelPuntuacion.SuspendLayout();
            this._panelP1.SuspendLayout();
            this.panelP1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelTablero
            // 
            this.panelTablero.Location = new System.Drawing.Point(24, 61);
            this.panelTablero.Name = "panelTablero";
            this.panelTablero.Size = new System.Drawing.Size(578, 570);
            this.panelTablero.TabIndex = 1;
            // 
            // _panelPuntuacion
            // 
            this._panelPuntuacion.BackColor = System.Drawing.Color.DeepSkyBlue;
            this._panelPuntuacion.Controls.Add(this.panelPuntuacion);
            this._panelPuntuacion.Location = new System.Drawing.Point(240, 200);
            this._panelPuntuacion.Name = "_panelPuntuacion";
            this._panelPuntuacion.Size = new System.Drawing.Size(207, 298);
            this._panelPuntuacion.TabIndex = 96;
            // 
            // panelPuntuacion
            // 
            this.panelPuntuacion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelPuntuacion.Controls.Add(this.labelPuntos);
            this.panelPuntuacion.Controls.Add(this.label8);
            this.panelPuntuacion.Controls.Add(this.labelNombre);
            this.panelPuntuacion.Controls.Add(this.labelMoves);
            this.panelPuntuacion.Controls.Add(this.btnNewGame);
            this.panelPuntuacion.Controls.Add(this.btnHome);
            this.panelPuntuacion.Controls.Add(this.btnAtras);
            this.panelPuntuacion.Controls.Add(this.label6);
            this.panelPuntuacion.Location = new System.Drawing.Point(4, 3);
            this.panelPuntuacion.Name = "panelPuntuacion";
            this.panelPuntuacion.Size = new System.Drawing.Size(200, 292);
            this.panelPuntuacion.TabIndex = 93;
            // 
            // labelNombre
            // 
            this.labelNombre.BackColor = System.Drawing.Color.Transparent;
            this.labelNombre.Font = new System.Drawing.Font("Georgia", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNombre.ForeColor = System.Drawing.Color.White;
            this.labelNombre.Location = new System.Drawing.Point(31, 15);
            this.labelNombre.Name = "labelNombre";
            this.labelNombre.Size = new System.Drawing.Size(146, 36);
            this.labelNombre.TabIndex = 4;
            this.labelNombre.Text = "Nombre";
            this.labelNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelMoves
            // 
            this.labelMoves.AutoSize = true;
            this.labelMoves.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMoves.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.labelMoves.Location = new System.Drawing.Point(95, 92);
            this.labelMoves.Name = "labelMoves";
            this.labelMoves.Size = new System.Drawing.Size(17, 18);
            this.labelMoves.TabIndex = 11;
            this.labelMoves.Text = "0";
            this.labelMoves.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNewGame
            // 
            this.btnNewGame.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnNewGame.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNewGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNewGame.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnNewGame.Location = new System.Drawing.Point(65, 213);
            this.btnNewGame.Name = "btnNewGame";
            this.btnNewGame.Size = new System.Drawing.Size(75, 23);
            this.btnNewGame.TabIndex = 9;
            this.btnNewGame.Text = "Reiniciar";
            this.btnNewGame.UseVisualStyleBackColor = false;
            this.btnNewGame.Click += new System.EventHandler(this.btnNewGame_Click);
            // 
            // btnHome
            // 
            this.btnHome.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHome.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnHome.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnHome.Location = new System.Drawing.Point(65, 247);
            this.btnHome.Name = "btnHome";
            this.btnHome.Size = new System.Drawing.Size(75, 23);
            this.btnHome.TabIndex = 8;
            this.btnHome.Text = "Inicio";
            this.btnHome.UseVisualStyleBackColor = false;
            this.btnHome.Click += new System.EventHandler(this.btnHome_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnAtras.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAtras.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAtras.ForeColor = System.Drawing.Color.Black;
            this.btnAtras.Location = new System.Drawing.Point(65, 178);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(75, 23);
            this.btnAtras.TabIndex = 7;
            this.btnAtras.Text = "Atrás";
            this.btnAtras.UseVisualStyleBackColor = false;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(62, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Movimientos";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe Script", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(219, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 34);
            this.label1.TabIndex = 2;
            this.label1.Text = "MOVIMIENTOS: ";
            // 
            // labelMovimientos
            // 
            this.labelMovimientos.AutoSize = true;
            this.labelMovimientos.Font = new System.Drawing.Font("Segoe Script", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMovimientos.Location = new System.Drawing.Point(402, 11);
            this.labelMovimientos.Name = "labelMovimientos";
            this.labelMovimientos.Size = new System.Drawing.Size(35, 40);
            this.labelMovimientos.TabIndex = 3;
            this.labelMovimientos.Text = "0";
            // 
            // _panelP1
            // 
            this._panelP1.BackColor = System.Drawing.Color.DeepSkyBlue;
            this._panelP1.Controls.Add(this.panelP1);
            this._panelP1.Location = new System.Drawing.Point(240, 200);
            this._panelP1.Name = "_panelP1";
            this._panelP1.Size = new System.Drawing.Size(177, 205);
            this._panelP1.TabIndex = 271;
            // 
            // panelP1
            // 
            this.panelP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panelP1.Controls.Add(this.buttonPuntuacionP1);
            this.panelP1.Controls.Add(this.textBoxNombreP1);
            this.panelP1.Controls.Add(this.label2);
            this.panelP1.Controls.Add(this.labelP1);
            this.panelP1.Location = new System.Drawing.Point(3, 3);
            this.panelP1.Name = "panelP1";
            this.panelP1.Size = new System.Drawing.Size(171, 199);
            this.panelP1.TabIndex = 94;
            // 
            // buttonPuntuacionP1
            // 
            this.buttonPuntuacionP1.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.buttonPuntuacionP1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPuntuacionP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPuntuacionP1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.buttonPuntuacionP1.Location = new System.Drawing.Point(44, 143);
            this.buttonPuntuacionP1.Name = "buttonPuntuacionP1";
            this.buttonPuntuacionP1.Size = new System.Drawing.Size(84, 23);
            this.buttonPuntuacionP1.TabIndex = 3;
            this.buttonPuntuacionP1.Text = "Puntuación";
            this.buttonPuntuacionP1.UseVisualStyleBackColor = false;
            this.buttonPuntuacionP1.Click += new System.EventHandler(this.buttonPuntuacionP1_Click);
            // 
            // textBoxNombreP1
            // 
            this.textBoxNombreP1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBoxNombreP1.Location = new System.Drawing.Point(37, 96);
            this.textBoxNombreP1.Name = "textBoxNombreP1";
            this.textBoxNombreP1.Size = new System.Drawing.Size(95, 20);
            this.textBoxNombreP1.TabIndex = 2;
            this.textBoxNombreP1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(28, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Introduzca su nombre :";
            // 
            // labelP1
            // 
            this.labelP1.AutoSize = true;
            this.labelP1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelP1.ForeColor = System.Drawing.Color.White;
            this.labelP1.Location = new System.Drawing.Point(40, 18);
            this.labelP1.Name = "labelP1";
            this.labelP1.Size = new System.Drawing.Size(87, 18);
            this.labelP1.TabIndex = 0;
            this.labelP1.Text = "VICTORIA!";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(62, 114);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 16);
            this.label8.TabIndex = 12;
            this.label8.Text = "Puntuación";
            // 
            // labelPuntos
            // 
            this.labelPuntos.AutoSize = true;
            this.labelPuntos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPuntos.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.labelPuntos.Location = new System.Drawing.Point(87, 147);
            this.labelPuntos.Name = "labelPuntos";
            this.labelPuntos.Size = new System.Drawing.Size(25, 13);
            this.labelPuntos.TabIndex = 14;
            this.labelPuntos.Text = "Pts";
            // 
            // Tablero
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MintCream;
            this.ClientSize = new System.Drawing.Size(612, 666);
            this.Controls.Add(this.labelMovimientos);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panelTablero);
            this.Controls.Add(this._panelP1);
            this.Controls.Add(this._panelPuntuacion);
            this.Name = "Tablero";
            this.Text = "Tablero";
            this.Load += new System.EventHandler(this.Tablero_Load);
            this.panelTablero.ResumeLayout(false);
            this._panelPuntuacion.ResumeLayout(false);
            this.panelPuntuacion.ResumeLayout(false);
            this.panelPuntuacion.PerformLayout();
            this._panelP1.ResumeLayout(false);
            this.panelP1.ResumeLayout(false);
            this.panelP1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelTablero;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label labelMovimientos;
        private System.Windows.Forms.Panel _panelPuntuacion;
        private System.Windows.Forms.Panel panelPuntuacion;
        private System.Windows.Forms.Label labelNombre;
        private System.Windows.Forms.Label labelMoves;
        private System.Windows.Forms.Button btnNewGame;
        private System.Windows.Forms.Button btnHome;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel _panelP1;
        private System.Windows.Forms.Panel panelP1;
        private System.Windows.Forms.Button buttonPuntuacionP1;
        private System.Windows.Forms.TextBox textBoxNombreP1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelP1;
        private System.Windows.Forms.Label labelPuntos;
        private System.Windows.Forms.Label label8;
    }
}